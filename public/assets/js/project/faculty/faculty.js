<<<<<<< HEAD
$(document).ready(function () {
//     function getListFaculties() {
//     let url = $('#data-faculties').data('url');
//     callAjax(url, 'get')
//         .then(function (data) {
//             $('#list-faculty').html(data);
//         });
// }
//
// getListFaculties();
    function notify(noti) {
        $('.noti-success').append('<div class="noti animate__animated animate__zoomInRight"\n' +
            'style="font-weight: bold; font-size: 20px; color:red;">' + noti + '</div>');
        $(".noti").delay(3000).slideUp();
    }

// create faculty
    $(document).on('click', '.add-faculty', function () {
        let url = $(this).data('url');
        let formData = new FormData($('#form-add-faculty')[0]);
        console.log(formData);
        callAjax(url, 'POST', formData)
            .then(function (data) {
                $('#modal-add-faculty').modal('hide');
                $('.data-table').load(' .data');
                $('.paginate').load(' .datas');
                notify('Create new faculty successfully!!');

            });

    });

// delete faculty
    $(document).on('click', '.btn-delete', function () {
        let url = $(this).attr('data-url');
        $('#modal-delete-faculty').modal('show');
        $('.delete-faculty').attr('data-url', url);
    });

    $(document).on('click', '.delete-faculty', function () {
        let url = $(this).attr('data-url');
        $('#modal-delete-faculty').modal('hide');
        callAjax(url, 'POST')
            .then(function (data) {
                $('.data-table').load(' .data');
                $('.paginate').load(' .datas');
                notify('Deleted faculty successfully!!');
            });
    });

//search faculty
//     function fetchData(search) {
//         $.ajax({
//             url: BASE_URL + "/faculties/list?search=" + search,
//             success: function (data) {
//                 $('.data tbody').html(data);
//             }
//         })
//     }

    $(document).on('keyup', '#search-faculty', function () {
        var search = $("#search-faculty").val();
        callAjax(url, 'GET', search)
            .then(function (data) {
                $('.list-faculty').html(data);
            })
    });
// end search


//update faculty
    $(document).on('click', '.btn-edit', function () {
        $('#edit-faculty').modal('show');
        var id = $(this).attr('data-id');
        let url = $(this).attr('data-url');
        callAjax(url, 'GET')
            .then(function (data) {
                $('#name-edit').val(data.faculty.name);
                $('#form-edit-faculty').attr('data-url',
                    'http://quanlysinhvien.com/faculties/' + data.faculty.id
                );
                $('.edit-faculty').attr('data-id', data.faculty.id);
                $('.edit-faculty').attr('data-url', url);
            })
    });

    $(document).on('click', '.edit-faculty', function (e) {

        let id = $(this).attr('data-id');
        let url = $(this).attr('data-url');
        let formData = new FormData($('#form-edit-faculty')[0]);
        callAjax(url, 'POST', formData)
            .then(function (data) {
                $('#edit-faculty').modal('hide');
                $('.data-table').load(' .data');
                notify('Updated faculty successfully!!');
            })
    });
    // end update
});



=======
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function() {

    // add 
    $("#form-add-faculty").submit(function(e) {
        e.preventDefault();
        var url = $(this).attr('data-url');
        $.ajax({
            type: "POST",
            url: url,
            data: {
                name: $('#name-add').val()
            },
            success: function(response) {
                $('#modal-add-faculty').modal('hide');
                $('.data-table').load(' .data');
                alert('Thêm khoa thành công!');
            }
        });
    });

    //delete
    $(document).on('click', '.btn-delete', function(e) {
        // var url = $(this).attr('data-url');
        var id = $(this).attr('data_id');
        if (confirm('Bạn có muốn xóa không ?')) {
            $.ajax({
                type: "DELETE",
                cache: false,
                url: 'faculties/' + id,
                success: function(response) {
                    // $('#check' + id).remove();
                    $('.data-table').load(' .data');
                }
            });
        }
    });

    //edit
    $(document).on('click', '.btn-edit', function (e) {
        e.preventDefault();
        $('#edit-faculty').modal('show');
        var url = $(this).attr('data-url');
        var id = $(this).attr('data-id');
        $.ajax({
            type: "GET",
            url: 'faculties/' + id + '/edit',
            cache: false,
            success: function(response) {
                $('#name-edit').val(response.faculty.name);
                $('#form-edit-faculty').attr('data-url',
                    '{{ asset("faculties/") }}/' + response.faculty.id);
                $('#form-edit-faculty').attr('data-id', response.faculty.id);
            }
        });
    });

    //update
    $('#form-edit-faculty').submit(function(e) {
        e.preventDefault();
        var url = $(this).attr('data-url');
        var id = $(this).attr('data-id');
        $.ajax({
            type: "PUT",
            url: 'faculties/' + id,
            data: {
                name: $('#name-edit').val()
            },
            success: function(response) {
                $('#edit-faculty').modal('hide');
                $('.data-table').load(' .data');
            }
        });
    });

    //search
    $('.btn-search').click(function(e) {
        e.preventDefault();
        var search = $('#data-faculty').val();
        $.ajax({
            type: "GET",
            url: "faculties/" + search,
            data: {
                search: search,
            },
            success: function(response) {
                $('#list-faculty').html(response);
            }
        });
    });
});
>>>>>>> 7201fec9505dfa660ab79ed1c2b892425f18978f
