<?php

namespace App\Http\Controllers;

use App\Http\Requests\Student\StoreStudentRequest;
use App\Http\Requests\Student\UpdateStudentRequest;
use App\Models\Faculty;
use App\Models\Student;
use App\Repositories\Student\StudentRepository;
use App\Traits\HandleImage;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StudentController extends Controller
{
    use HandleImage;

    public $studentRepository;

    public function __construct(StudentRepository $student)
    {
        $this->studentRepository = $student;
    }

    /**
     * Display a listing of the resource.
     *
     *
     */
    public function index()
    {
        $faculties = Faculty::All();
        $students = $this->studentRepository->getWithPaginate($quantity = 5);
        return view('admin.pages.student.index', [
            'faculties' => $faculties,
            'students' => $students,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(): Response
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreStudentRequest $request
     * @return JsonResponse
     */
    public function store(StoreStudentRequest $request): JsonResponse
    {
        $data = $request->all();
        $data['avatar'] = $this->moveImage($request->avatar);
        $student = Student::create($data);
        return response()->json([
            'message' => 'Add student successfully',
            'student' => $student
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {

        // eager loading
        $student = Student::where('id', $id)->with('faculty')->first();
        return response()->json([
            'student' => $student,
            'message' => 'Get student successfully',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function edit(int $id): JsonResponse
    {
        $student = Student::findOrFail($id);
        return response()->json([
            'student' => $student,
            'message' => 'Get student successfully',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */

    public function update(Request $request, int $id): JsonResponse
    {
        $student = Student::findOrFail($id);
        $data = $request->all();
        $data['avatar'] = $this->updateImage($request, $student->avatar);
        $student->update($data);
        return response()->json([
            'message' => 'updated student successfully',
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $student = Student::findOrFail($id)->delete();
        return response()->json([
            'message' => 'Deleted student successfully',
            'student' => $student
        ], 200);
    }
}
