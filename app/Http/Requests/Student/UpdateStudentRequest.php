<?php

namespace App\Http\Requests\student;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:150',
            'name' => 'required|min:5|max:150',
            'birthday' => 'required',
            'address' => 'required',
            'phone' => 'required|min:10|max:150',
            'gender' => 'required',
            'faculty_id' => 'required',
        ];
    }

}
