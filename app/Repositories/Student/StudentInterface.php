<?php
namespace App\Repositories\Student;

interface StudentInterface
{
    public function getAll();

    public function create($attributes, $imageName);

    public function handUpdate($request);

    public function update($id, $attributes);

    public function delete($id);
}
?>