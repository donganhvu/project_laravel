<?php

namespace App\Repositories;

use Illuminate\Contracts\Container\BindingResolutionException;

abstract class BaseRepository
{
    protected $model;

    abstract protected function model();

    /**
     * @throws BindingResolutionException
     */
    public function __construct()
    {
        $this->makeModel();
    }

    /**
     * @throws BindingResolutionException
     */
    protected function makeModel()
    {
        $this->model = app()->make($this->model());
    }
}
