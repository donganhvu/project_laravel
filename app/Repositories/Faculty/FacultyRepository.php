<?php

namespace App\Repositories\Faculty;

use App\Models\Faculty;
use App\Repositories\BaseRepository;

/**
 * @method findOrFail(int $id)
 */
class FacultyRepository extends BaseRepository
{
    protected function model(): string
    {
        return Faculty::class;
    }

    public function getWithPaginate($quantity = 5)
    {

        return $this->model->latest('id')->paginate($quantity);
    }

    public function create($request)
    {
        return $this->model->create($request);
    }

    public function update($id, $request)
    {

        return $this->model->findOrFail($id)->update([
            'name' => $request->name,
        ]);
    }

//    public function search($request)
//    {
////        if (empty($request)) {
////            $faculty = $this->model->latest('id')->paginate(5);
////        } else {
//        return $this->model->query()
//            ->where('id', 'LIKE', "%{$request->search}%")
//            ->orWhere('name', 'LIKE', "%{$request->search}%")
//            ->get();
//    }

    public function search($request): string
    {
        $faculties = Faculty::query()
            ->where('id', 'LIKE', "%{$request->search}%")
            ->orWhere('name', 'LIKE', "%{$request->search}%")
            ->get();
        return $this->result($faculties);
    }

    public function result($faculties): string
    {
        $output = '';
        if (count($faculties) > 0) {
            $count = 1;
            foreach ($faculties as $faculty) {
                $output .= '<tr id="check' . $faculty->id . '">';
                $output .= '<td>' . $count++ . '</td>';
                $output .= '<td>' . $faculty->id . '</td>';
                $output .= '<td>' . $faculty->name . '</td>';
                if ($faculty->status == 1) {
                    $output .= '<td>' . 'Đang hoạt động' . '</td>';
                } else {
                    $output .= '<td>' . 'Dừng hoạt động' . '</td>';
                }
                $output .= '<td>' . $faculty->created_at . '</td>';
                $output .= '<td>' . "<button class='btn btn-danger btn-edit'
                 data-url='{{ route(" . 'faculties.edit' . ", $faculty->id) }}'>Sửa</button>" . '</td>';
                $output .= '<td>' . "<button class='btn btn-warning btn-delete'
                 data-url='{{ route(" . 'faculties.destroy' . ", $faculty->id) }}'>Xóa</button>" . '</td>';
                $output .= '</tr>';
            }
        } else {
            $output .= '<tr>' . '<td colspan="7">' . "Không tìm thấy thông tin nào" . '</td>' . '</tr>';
        }
        return $output;
    }

}
