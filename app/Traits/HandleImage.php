<?php

namespace App\Traits;

trait HandleImage
{
    public $path = "assets/uploads/students/";

    public function getFileName($file): string
    {
        return time() . '_' . $file->getClientOriginalName();
    }

    public function getFileSize($file)
    {
        return $file->getSize();
    }


    public function deleteImage($imageCurrent, $path)
    {
        $path = $this->path;
        if (file_exists($path . $imageCurrent)) {
            unlink($path . $imageCurrent);
        }
    }

    public function updateImage($request, $imageCurrent)
    {
        $path = $this->path;
        $request->hasFile('avatar') ? $file = $request->file('avatar') : $imageName = $imageCurrent;
        if (isset($file)) {
            $imageName = $this->getFileName($file);
            $this->deleteImage($imageCurrent, $path);
            $this->moveImage($file);
        }
        return $imageName;
    }

    public function moveImage($file)
    {
        $path = $this->path;
        $imageName = $this->getFileName($file);
        $file->move($path, $imageName);
        return $imageName;
    }
}


