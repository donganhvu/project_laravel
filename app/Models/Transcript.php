<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transcript extends Model
{
    protected $table = "faculties";

    protected $primaryKey = "id";

    protected $fillable = [
        'id', 'student_id', 'subject_id', 'exam_number', 'point'
    ];
}
