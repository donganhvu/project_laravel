@foreach ($students as $student)
    <tr>
        <td>{{ $student->id }}</td>
        <td>{{ $student->name }}</td>
        <td>
            <img width="100" height="100"
                 src="{{ asset('assets/uploads/students/' . $student->avatar) }}" alt="">
        </td>
{{--        <td>{{ $student->faculty->name }}</td>--}}
        @if ($student->status == 1)
            <td style="color: green; font-weight: bold">Đang học</td>
        @else
            <td style="color: red ;font-weight: bold">Thôi học</td>
        @endif
        <td>
            <button type="button" data-id="{{ $student->id }}"
                    data-url="{{ route('students.show', $student->id) }}"
                    class="btn btn-info btn-detail-student">
                <i class="fas fa-info-circle"></i>
            </button>
        </td>
        <td>
            <button type="button" data-id="{{ $student->id }}"
                    data-url="{{ route('students.edit', $student->id) }}"
                    data-action="{{ route('students.update', $student->id) }}"
                    class="btn btn-warning btn-edit-student"><i
                    class="fas fa-exclamation-triangle"></i></button>
        </td>
        <td>
            <button class="btn btn-danger btn-delete-student" data-id="{{ $student->id }}"
                    data-url="{{ route('students.delete', $student->id) }}"><i
                    class="fas fa-trash"></i></button>
        </td>
    </tr>
@endforeach
