<div class="modal fade" id="modal-add-student" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">New Student</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form data-url="{{ route('students.store') }}" method="POST" id="form-add-student"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
<<<<<<< HEAD:resources/views/admin/pages/student/create.blade.php
                        <label for="name">Name</label>
=======
                        <label for="name">Tên sinh viên : <span style="color:red;font-weight: bold" id="errorName"></span></label>
>>>>>>> 7201fec9505dfa660ab79ed1c2b892425f18978f:resources/views/pages/student/add-student.blade.php
                        <input type="text" class="form-control" id="name" name="name" placeholder="Nhập tên sinh viên">

                        <br>
                        <label for="avatar">Avatar</label>
                    </div>
                    <div class="custom-file">
<<<<<<< HEAD:resources/views/admin/pages/student/create.blade.php
                        <input type="file" class="custom-file-input" id="avatar" name="avatar">
                        <label class="custom-file-label" for="customFile">Chose</label>
=======
                        <input type="file" class="custom-file-input" accept="image/*" id="avatar" name="avatar">
                        <span style="color:red;font-weight: bold" id="errorAvatar"></span>
                        <label class="custom-file-label" for="customFile">Chọn Ảnh</label>
>>>>>>> 7201fec9505dfa660ab79ed1c2b892425f18978f:resources/views/pages/student/add-student.blade.php
                    </div>
                    <div class="form-group">
                        <img id="image" style="margin: 10px; border-radius:5px; box-shadow: 3px 3px 7px grey" />
                    </div>
                    <div class="form-group">
                        <label for="faculty_id">Khoa</label>
                        <select name="faculty_id" class="form-control" id="">
                            @foreach ($faculties as $faculty)
                                <option value="{{ $faculty->id }}">{{ $faculty->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="birthday">Ngày sinh : <span style="color:red;font-weight: bold"
                                id="errorBirthday"></span></span></label>
                        <input type="date" class="form-control" id="birthday" name="birthday">

                    </div>
                    <div class="form-group">
                        <label for="address">Địa chỉ :<span style="color:red;font-weight: bold"
                                id="errorAddress"></span></label>
                        <input type="text" class="form-control" id="address" name="address"
                            placeholder="Nhập tên địa chỉ...">

                    </div>
                    <div class="form-group">
                        <label for="phone">Số điện thoại : <span style="color:red;font-weight: bold" id="errorPhone"></span></label>
                        <input type="text" class="form-control" id="phone" name="phone"
                            placeholder="Nhập số điện thoại...">

                    </div>
                    <div class="form-group">
                        <label for="gender">Giới tính</label><br>
                        <input type="radio" style="margin-left:10px" value="1" checked name="gender"> Nam
                        <input type="radio" style="margin-left:10px" value="2" name="gender"> Nữ
                        <input type="radio" style="margin-left:10px" value="3" name="gender"> Khác
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary add-student" data-url="{{ route('students.store') }}">Create</button>
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
