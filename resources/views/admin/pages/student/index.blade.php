@extends('index')
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Manager Students</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="">Home</a></li>
            <li class="breadcrumb-item">Tables</li>
            <li class="breadcrumb-item active" aria-current="page">DataTables</li>
        </ol>
    </div>

    <!-- Row -->
    <div class="row">
        <!-- DataTable with Hover -->
        <div class="col-lg-12">
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h2 class="m-0 font-weight-bold text-primary">List Students</h2>
                </div>
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <button style="width: 200px ;font-weight: bold" type="button" class="btn btn-success"
                            data-toggle="modal" data-target="#modal-add-student" id="#modal-add-student">Thêm sinh viên
                    </button>
                    @if (session()->has('noti'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('noti') }}
                        </div>
                    @endif
                </div>
                <div class="noti-success" style="margin-left:50px">

                </div>
                <div class="table-responsive p-3 data-table">
                    <table class="table align-items-center table-flush table-hover data" style="text-align: center">
                        <thead class="thead-light">
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Avatar</th>
                            <th>Faculty</th>
                            <th>Status</th>
                            <th>View</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Avatar</th>
                            <th>Faculty</th>
                            <th>Status</th>
                            <th>View</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </tfoot>
                        <tbody class="list-faculty">
                        @include('admin.pages.student.list')
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="paginate">
        <div class="datas">{{ $students->links() }}</div>
    </div>
    {{-- modal add-student --}}
    @include('admin.pages.student.create')
    @include('admin.pages.student.show')
    @include('admin.pages.student.edit')
    @include('admin.pages.student.delete')

@endsection
@section('script')
    <script src="{{ asset('assets/js/project/student/student.js')}}"></script>
@endsection
