@php
    $count = 1;
@endphp
@foreach ($faculties as $faculty)
    <tr id="check{{ $faculty->id }}">
        <td>{{ $count++ }}</td>
        <td>{{ $faculty->id }}</td>
        <td>{{ $faculty->name }}</td>
        <td>
            @if ($faculty->status == 1)
                Đang hoạt động
            @else
                Dừng hoạt động
            @endif
        </td>
        <td>{{ $faculty->created_at }}</td>
        <td>
            <button class="btn btn-danger btn-edit" data-id="{{$faculty->id}}"
                    data-url="{{ route('faculties.update', $faculty->id) }}">Sửa
            </button>
        </td>
        <td>
            <button data_id="{{ $faculty->id }}"
                    data-url="{{ route('faculties.delete', $faculty->id) }}"
                    class="btn btn-warning btn-delete">Xóa
            </button>
        </td>
    </tr>
@endforeach
