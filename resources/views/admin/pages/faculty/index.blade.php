@extends('index')
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Manager Faculty</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="">Home</a></li>
            <li class="breadcrumb-item">Tables</li>
            <li class="breadcrumb-item active" aria-current="page">DataTables</li>
        </ol>
    </div>

    <!-- Row -->
    <div class="row">
        <!-- DataTable with Hover -->
        <div class="col-lg-12">
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h2 class="m-0 font-weight-bold text-primary" style="text-align: center">List Faculty</h2>
                </div>
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <button style="width: 200px ;font-weight: bold" type="button" class="btn btn-success"
                            data-toggle="modal" data-target="#modal-add-faculty" id="#modal-add-faculty">Add Faculty
                    </button>
                    @if (session()->has('notify'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('notify') }}
                        </div>
                    @endif
                </div>
                <div style="width: 350px; display: flex; flex-direction: row; line-height: 30px;margin-left:751px;">
                    <div>
                        <button class="btn btn-info btn-search" style="height:30px; line-height: 18px;">Search</button>
                    </div>
                    <div>
                        <input type="text" placeholder="Enter value..." data-url="{{ route('faculties.list') }}"
                               id="search-faculty"
                               class="form-control form-control-sm">
                    </div>
                </div>
                <div class="noti-success" style="margin-left:50px">

                </div>
                <div class="data-table">
                    <div class="table-responsive p-3 data">
                        <table id="check" class="table align-items-center table-flush table-hover"
                               style="text-align: center">
                            <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Created_at</th>
                                <th colspan="2">Option</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Created_at</th>
                                <th colspan="2">Option</th>
                            </tr>
                            </tfoot >
                            <tbody class="list-faculty">
                            @php
                                $count = 1;
                            @endphp
                            @foreach ($faculties as $faculty)
                                <tr id="check{{ $faculty->id }}">
                                    <td>{{ $count++ }}</td>
                                    <td>{{ $faculty->id }}</td>
                                    <td>{{ $faculty->name }}</td>
                                    <td>
                                        @if ($faculty->status == 1)
                                            Đang hoạt động
                                        @else
                                            Dừng hoạt động
                                        @endif
                                    </td>
                                    <td>{{ $faculty->created_at }}</td>
                                    <td>
                                        <button class="btn btn-danger btn-edit" data-id="{{$faculty->id}}"
                                                data-url="{{ route('faculties.update', $faculty->id) }}">Sửa
                                        </button>
                                    </td>
                                    <td>
                                        <button data_id="{{ $faculty->id }}"
                                                data-url="{{ route('faculties.delete', $faculty->id) }}"
                                                class="btn btn-warning btn-delete">Xóa
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            <tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Row-->
    <div class="paginate">
        <div class="datas">{{ $faculties->links() }}</div>
    </div>



    <!-- Modal add faculty -->
    @include('admin.pages.faculty.create')
    <!-- Modal add faculty -->

    {{-- Modal edit faculty --}}
    @include('admin.pages.faculty.edit')
    {{-- End modal --}}

    {{-- Modal edit faculty --}}
    @include('admin.pages.faculty.delete')
    {{-- End modal --}}

@endsection
@section('script')
    <script src="{{ asset('assets/js/project/faculty/faculty.js')}}"></script>
@endsection
