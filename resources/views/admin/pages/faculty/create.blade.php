<div class="modal fade" id="modal-add-faculty" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">New Faculty</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" id="form-add-faculty">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Faculty name</label>
                        <input type="text" class="form-control" id="name-add" name="name" placeholder="Nhập tên khoa">
                        @error('name')
                        <span>{{ $message }}</span>
                        @enderror
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary add-faculty" data-url="{{ route('faculties.store') }}">
                    Create Faculty
                </button>
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
