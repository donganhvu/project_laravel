@extends('index')
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Manager Subject </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="">Home</a></li>
            <li class="breadcrumb-item">Tables</li>
            <li class="breadcrumb-item active" aria-current="page">DataTables</li>
        </ol>
    </div>

    <!-- Row -->
    <div class="row">
        <!-- DataTable with Hover -->
        <div class="col-lg-12">
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h2 class="m-0 font-weight-bold text-primary" style="text-align: center">List Subject</h2>
                </div>
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <button style="width: 200px ;font-weight: bold" type="button" class="btn btn-success"
                        data-toggle="modal" data-target="#modal-add-faculty" id="#modal-add-faculty">Add Subject</button>
                    @if (session()->has('notify'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('notify') }}
                        </div>
                    @endif
                </div>
                <div style="width: 350px; display: flex; flex-direction: row; line-height: 30px;margin-left:751px;">
                    <div><button class="btn btn-info btn-search" style="height:30px; line-height: 18px;">Search</button>
                    </div>
                    <div><input type="text" placeholder="..." id="data-faculty" class="form-control form-control-sm">
                    </div>
                </div>
                <div class="data-table">
                    <div class="table-responsive p-3 data">
                        <table id="check" class="table align-items-center table-flush table-hover"
                            style="text-align: center">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Credit</th>
                                    <th>Created_at</th>
                                    <th colspan="2">Option</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Credit</th>
                                    <th>Created_at</th>
                                    <th colspan="2">Option</th>
                                </tr>
                            </tfoot>
                            <tbody id="list-faculty">
                                @php
                                    $count = 1;
                                @endphp
                                @foreach ($subjects as $subject)
                                    <tr id="check{{ $subject->id }}">
                                        <td>{{ $count++ }}</td>
                                        <td>{{ $subject->id }}</td>
                                        <td>{{ $subject->name }}</td>
                                        <td>{{ $subject->credit }}</td>
                                        <td>{{ $subject->created_at }}</td>
                                        <td>
                                            <button class="btn btn-danger btn-edit" data-id="{{$subject->id}}"
                                                data-url="{{ route('faculties.edit', $subject->id) }}">Sửa</button>
                                        </td>
                                        <td>
                                            <button data_id="{{ $subject->id }}"
                                                data-url="{{ route('faculties.destroy', $subject->id) }}"
                                                class="btn btn-warning btn-delete">Xóa</button>
                                        </td>
                                    </tr>
                                @endforeach
                            <tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--Row-->
    {{ $subjects->links() }}


    <!-- Modal add faculty -->
    @include('admin.pages.subject.add-subject')
    <!-- Modal add faculty -->

    {{-- Modal edit faculty --}}
    @include('admin.pages.subject.edit-subject')
    {{-- End modal --}}


@endsection
@section('script')
<<<<<<< HEAD:resources/views/admin/pages/subject/view-subject.blade.php
    {{-- <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function() {

            // add
            $("#form-add-faculty").submit(function(e) {
                e.preventDefault();
                var url = $(this).attr('data-url');
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        name: $('#name-add').val()
                    },
                    success: function(response) {
                        $('#modal-add-faculty').modal('hide');
                        $('.data-table').load(' .data');
                        alert('Thêm khoa thành công!');
                    }
                });
            });

            //delete
            $(document).on('click', '.btn-delete', function(e) {
                // var url = $(this).attr('data-url');
                var id = $(this).attr('data_id');
                if (confirm('Bạn có muốn xóa không ?')) {
                    $.ajax({
                        type: "DELETE",
                        cache: false,
                        url: 'faculties/' + id,
                        success: function(response) {
                            // $('#check' + id).remove();
                            $('.data-table').load(' .data');
                        }
                    });
                }
            });

            //edit
            $(document).on('click', '.btn-edit', function (e) {
                e.preventDefault();
                $('#edit-faculty').modal('show');
                var url = $(this).attr('data-url');
                var id = $(this).attr('data-id');
                $.ajax({
                    type: "GET",
                    url: 'faculties/' + id + '/edit',
                    cache: false,
                    success: function(response) {
                        $('#name-edit').val(response.faculty.name);
                        $('#form-edit-faculty').attr('data-url',
                            '{{ asset('faculties/') }}/' + response.faculty.id);
                        $('#form-edit-faculty').attr('data-id', response.faculty.id);
                    }
                });
            });

            //update
            $('#form-edit-faculty').submit(function(e) {
                e.preventDefault();
                var url = $(this).attr('data-url');
                var id = $(this).attr('data-id');
                $.ajax({
                    type: "PUT",
                    url: 'faculties/' + id,
                    data: {
                        name: $('#name-edit').val()
                    },
                    success: function(response) {
                        $('#edit-faculty').modal('hide');
                        $('.data-table').load(' .data');
                    }
                });
            });

            //search
            $('.btn-search').click(function(e) {
                e.preventDefault();
                var search = $('#data-faculty').val();
                $.ajax({
                    type: "GET",
                    url: "{{ route('faculties.show', 'search') }}",
                    data: {
                        search: search,
                    },
                    success: function(response) {
                        $('#list-faculty').html(response);
                    }
                });
            });
        });

    </script> --}}
=======
    <script src="{{ asset('assets/js/project/faculty/faculty.js') }}"></script>
>>>>>>> 7201fec9505dfa660ab79ed1c2b892425f18978f:resources/views/pages/faculty/view-faculty.blade.php
@endsection

