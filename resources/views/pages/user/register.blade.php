<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.head')
</head>

<body class="bg-gradient-login">
    <!-- Register Content -->
    <div class="container-login">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-12 col-md-9">
                <div class="card shadow-sm my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="login-form">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4" style="font-weight: bold;">Register</h1>
                                    </div>
                                    <form action="{{ route('store') }}" method="POST">
                                        @csrf
                                        <div class="form-group">
                                            <label style="font-weight: bold;">Username</label>
                                            <input type="email" class="form-control" name="email"
                                                id="exampleInputFirstName" placeholder="Enter username">
                                        </div>
                                        <div class="form-group">
                                            <label style="font-weight: bold;">Password</label>
                                            <input type="password" class="form-control" name="password"
                                                id="exampleInputPassword" placeholder="Enter pass">
                                        </div>
                                        <div class="form-group">
                                            <label style="font-weight: bold;">Re-Password</label>
                                            <input type="password" class="form-control" name="re_pass"
                                                id="exampleInputPassword" placeholder="Enter re-pass">
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary btn-block">Register</button>
                                        </div>
                                        <hr>
                                        <a href="index.html" class="btn btn-google btn-block">
                                            <i class="fab fa-google fa-fw"></i> Register with Google
                                        </a>
                                        <a href="index.html" class="btn btn-facebook btn-block">
                                            <i class="fab fa-facebook-f fa-fw"></i> Register with Facebook
                                        </a>
                                    </form>
                                    <hr>
                                    <div class="text-center">
                                        <a class="font-weight-bold small" href="{{ route('index') }}">Already have an
                                            account?</a>
                                    </div>
                                    <div class="text-center">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Register Content -->
    @include('includes.script')
</body>

</html>
