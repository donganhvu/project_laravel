@extends('index')
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Manager Students</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item">Tables</li>
            <li class="breadcrumb-item active" aria-current="page">DataTables</li>
        </ol>
    </div>

    <!-- Row -->
    <div class="row">
        <!-- DataTable with Hover -->
        <div class="col-lg-12">
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h2 class="m-0 font-weight-bold text-primary">List Students</h2>
                </div>
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <button style="width: 200px ;font-weight: bold" type="button" class="btn btn-success"
                        data-toggle="modal" data-target="#modal-add-student" id="#modal-add-student">Thêm sinh viên</button>
                    @if (session()->has('noti'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('noti') }}
                        </div>
                    @endif
                </div>
                <div class="table-responsive p-3 data-table">
                    <table class="table align-items-center table-flush table-hover data" style="text-align: center">
                        <thead class="thead-light">
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Avatar</th>
                                <th>Faculty</th>
                                <th>Status</th>
                                <th>View</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Avatar</th>
                                <th>Faculty</th>
                                <th>Status</th>
                                <th>View</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach ($students as $student)
                                <tr>
                                    <td>{{ $student->id }}</td>
                                    <td>{{ $student->name }}</td>
                                    <td>
                                        <img width="100" height="100"
                                            src="{{ asset('assets/uploads/students/' . $student->avatar) }}" alt="">
                                    </td>
                                    <td>{{ $student->faculty->name }}</td>
                                    @if ($student->status == 1)
                                        <td style="color: green; font-weight: bold">Đang học</td>
                                    @else
                                        <td style="color: red ;font-weight: bold">Thôi học</td>
                                    @endif
                                    <td>
                                        <button type="button" data-id="{{ $student->id }}"
                                            data-url="{{ route('students.show', $student->id) }}"
                                            class="btn btn-info btn-detail-student">
                                            <i class="fas fa-info-circle"></i>
                                        </button>
                                    </td>
                                    <td>
                                        <button type="button" data-id="{{ $student->id }}"
                                            data-url="{{ route('students.edit', $student->id) }}"
                                            class="btn btn-warning btn-edit-student"><i
                                                class="fas fa-exclamation-triangle"></i></button>
                                    </td>
                                    <td>
                                        <button class="btn btn-danger btn-delete-student" data-id="{{ $student->id }}"
                                            data-url="{{ route('students.destroy', $student->id) }}"><i
                                                class="fas fa-trash"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{ $students->links() }}
    {{-- modal add-student --}}
    @include('pages/student.add-student')
    @include('pages/student.detail-student')
    @include('pages/student.edit-student')

@endsection
@section('script')
    <script src="{{ asset('assets/js/project/student/student.js') }}"></script>
    <script>
        //setup csrf token 
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(function() {
            $('#form-add-student').submit(function(e) {
                e.preventDefault();
                validateAdd();
                var url = $(this).attr('data-url');
                let formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: url,
                    data: formData,
                    contentType: false,
                    processData: false,
                    cache: false,
                    success: function(response) {
                        // location.reload();
                        $('.data-table').load(' .data');
                        $('#modal-add-student').modal('hide');
                    }
                });
            });

            // fix multi images
            $(".btn-close-modal").click(function(e) {
                e.preventDefault();
                $(".modal-load").load(" .avatar-view");
                $(".modal-loads").load(" .avatar-views");
            });

            //detail-student
            $(document).on('click', '.btn-detail-student', function() {
                $('#detail-student').modal('show');
                var url = $(this).attr('data-url');
                var id = $(this).attr('data-id');
                $.ajax({
                    type: "GET",
                    url: 'students/' + id,
                    success: function(response) {
                        var gender, status;
                        //check gender
                        if (response.student.gender == 1) {
                            gender = 'Nam';
                        } else if (response.student.gender == 2) {
                            gender = 'Nữ';
                        } else {
                            gender = '3D';
                        }

                        //check status 
                        if (response.student.status == 1) {
                            status = "Đang học";
                        } else {
                            status = "Nghỉ học";
                        }

                        //view detail-student
                        $('#name-view').html(response.student.name);
                        $('#faculty-view').html(response.faculty.name);
                        $('#gender-view').html(gender);
                        $('#birthday-view').html(response.student.birthday);
                        $('#address-view').html(response.student.address);
                        $('#phone-view').html(response.student.phone);
                        $('#status-view').html(status);
                        $('.avatar-view').append(
                            '<img style="border-radius: 5px; box-shadow: 3px 3px 7px grey; width: 150px ;height: 150px; margin: 15px" src="{{ asset('assets/uploads/students/') }}/' +
                            response.student.avatar + '">');
                    }
                });
            });


            // delete student
            $(document).on('click', '.btn-delete-student', function() {
                var url = $(this).attr('data-url');
                var id = $(this).attr('data-id');
                if (confirm('Bạn có muốn xóa không ?')) {
                    $.ajax({
                        type: "DELETE",
                        url: 'students/' + id,
                        success: function(response) {
                            // location.reload();
                            $('.data-table').load(' .data');
                        }
                    });
                }
            });

            //show edit student
            $(document).on('click', '.btn-edit-student', function(e) {
                e.preventDefault();
                $('#modal-edit-student').modal('show');
                var url = $(this).attr('data-url');
                var id = $(this).attr('data-id');          
                $.ajax({
                    type: "GET",
                    url: 'students/' + id,
                    success: function(response) {
                        var faculty_id = response.student.faculty_id;
                        var gender = response.student.gender;
                        $('#faculty_id' + faculty_id).attr('selected', 'true');
                        $('#name_edit').val(response.student.name);
                        $('#birthday-edit').val(response.student.birthday);
                        $('#address-edit').val(response.student.address);
                        $('#phone-edit').val(response.student.phone);
                        $('#avatar_current').val(response.student.avatar);
                        $('.avatar-views').append(
                            '<img style="border-radius: 5px; box-shadow: 3px 3px 7px grey; width: 150px ;height: 150px; margin: 15px" src=" {{ asset('assets/uploads/students/') }}/' +
                            response.student.avatar + ' ">');

                        $('input[name=gender][value=' + gender + ']').attr('checked', true);
                        $('#form-edit-student').attr('action', '{{ asset('students/') }}/' +
                            response.student.id)
                    }
                });
            });

            $("#avatar").change(function(e) {
                e.preventDefault();
                $('#image').attr({
                    'width': '150',
                    'height': '150'
                });
                var reader = new FileReader();
                reader.onload = function(e) {
                    // get loaded data and render thumbnail.
                    document.getElementById("image").src = e.target.result;
                };
                // read the image file as a data URL.
                reader.readAsDataURL(this.files[0]);
            });

            $('#avatar_new').change(function(e) {
                e.preventDefault();
                $('#images').attr({
                    'width': '150',
                    'height': '150'
                });
                var reader = new FileReader();
                reader.onload = function(e) {
                    // get loaded data and render thumbnail.
                    document.getElementById("images").src = e.target.result;
                };
                // read the image file as a data URL.
                reader.readAsDataURL(this.files[0]);
            });
        });

    </script>
@endsection
